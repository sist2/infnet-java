package br.com.infnet.exceptions;

public class SolicitanteException extends Exception{
	
	private static final long serialVersionUID = 1L;

	protected String nome;
	
	public SolicitanteException(String nome) {
		super();
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "Nome não pode ser nula";
	}
	
}
