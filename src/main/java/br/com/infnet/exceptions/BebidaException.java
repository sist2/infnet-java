package br.com.infnet.exceptions;

public class BebidaException extends Exception {
	
	private static final long serialVersionUID = 1L;

	protected String marca;
	
	public BebidaException(String marca) {
		super();
		this.marca = marca;
	}
	
	@Override
	public String toString() {
		return "Marca não pode ser nula";
	}
	
}
