package br.com.infnet.exceptions;

import java.math.BigDecimal;

public class ComidaException extends Exception{
	
	private static final long serialVersionUID = 1L;

	protected BigDecimal peso;
	
	public ComidaException(BigDecimal peso) {
		super();
		this.peso = peso;
	}
	
	@Override
	public String toString() {
		return "Peso não pode ser negativo ou nulo";
	}
	
}
