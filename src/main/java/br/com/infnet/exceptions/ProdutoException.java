package br.com.infnet.exceptions;

import java.math.BigDecimal;

public class ProdutoException extends Exception{
	
	private static final long serialVersionUID = 1L;

	protected BigDecimal valor;
	
	public ProdutoException(BigDecimal valor) {
		super();
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return "Valor não pode ser negativo ou nulo";
	}
	
}
