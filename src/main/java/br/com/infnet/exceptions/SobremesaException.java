package br.com.infnet.exceptions;

import java.math.BigDecimal;

public class SobremesaException extends Exception{
	
	private static final long serialVersionUID = 1L;

	protected BigDecimal quantidade;
	
	public SobremesaException(BigDecimal quantidade) {
		super();
		this.quantidade = quantidade;
	}
	
	@Override
	public String toString() {
		return "Quantidade não pode ser negativo ou nulo";
	}
	
}
