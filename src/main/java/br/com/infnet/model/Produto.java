package br.com.infnet.model;

import java.math.BigDecimal;

import br.com.infnet.exceptions.ProdutoException;

public abstract class Produto {
	
	private String nome;
	
	private BigDecimal valor;
	
	private Integer codigo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public abstract String getTipo();
	
	@Override
	public String toString() {
		return "Produto [nome=" + nome + ", valor=" + valor + ", codigo=" + codigo + "]";
	}
	
	public void verificaValor() {
		try {
			if(this.valor == null || BigDecimal.ZERO.compareTo(this.valor) == 1) {
				throw new ProdutoException(this.valor);
			}
		}
		catch(ProdutoException e) {
			e.printStackTrace();
		}
	}
}
