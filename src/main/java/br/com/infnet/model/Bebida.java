package br.com.infnet.model;

import br.com.infnet.exceptions.BebidaException;

public class Bebida extends Produto{
	
	private boolean gelada;
	
	private float tamanho;
	
	private String marca;

	public boolean isGelada() {
		return gelada;
	}

	public void setGelada(boolean gelada) {
		this.gelada = gelada;
	}

	public float getTamanho() {
		return tamanho;
	}

	public void setTamanho(float tamanho) {
		this.tamanho = tamanho;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "Bebida [gelada=" + gelada + ", tamanho=" + tamanho + ", marca=" + marca + "]";
	}

	@Override
	public String getTipo() {		
		return "Bebida da marca: "+marca;
	}
	
	public void verificaMarca() {
		try {
			if(this.marca == null) {
				throw new BebidaException(this.marca);
			}
		}
		catch(BebidaException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
