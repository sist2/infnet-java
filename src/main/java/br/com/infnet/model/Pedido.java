package br.com.infnet.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.infnet.exceptions.BebidaException;
import br.com.infnet.exceptions.PedidoException;

public class Pedido {
	
	private String descricao;
	
	private boolean web;
	
	private LocalDateTime data;
	
	private List<Produto> produtos = new ArrayList<Produto>();
	
	private Solicitante solicitante;
	
	private Set<String> historicoDescricoes = new HashSet<String>();

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isWeb() {
		return web;
	}

	public void setWeb(boolean web) {
		this.web = web;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Solicitante getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}
	
	public Set<String> getHistoricoDescricoes() {
		return historicoDescricoes;
	}

	public void setHistoricoDescricoes(Set<String> historicoDescricoes) {
		this.historicoDescricoes = historicoDescricoes;
	}

	@Override
	public String toString() {
		return "Pedido [descricao=" + descricao + ", web=" + web + ", data=" + data + ", produtos=" + produtos
				+ ", solicitante=" + solicitante + "]";
	}
	
	public void verificaPedido() {
		try {
			if(this.descricao == null) {
				throw new PedidoException(this.descricao);
			}
		}
		catch(PedidoException e) {
			e.printStackTrace();
		}
	}
	
	
}
