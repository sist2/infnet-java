package br.com.infnet.model;

import java.math.BigDecimal;

import br.com.infnet.exceptions.BebidaException;
import br.com.infnet.exceptions.ComidaException;

public class Comida extends Produto{
	
	private BigDecimal peso;
	
	private boolean vegano;
	
	private String ingredientes;
	
	private int quantidadeVendasVegano[];
	
	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public boolean isVegano() {
		return vegano;
	}

	public void setVegano(boolean vegano) {
		this.vegano = vegano;
	}

	public String getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}	

	public int[] getQuantidadeVendasVegano() {
		return quantidadeVendasVegano;
	}

	public void setQuantidadeVendasVegano(int[] quantidadeVendasVegano) {
		this.quantidadeVendasVegano = quantidadeVendasVegano;
	}

	@Override
	public String toString() {
		return "Comida [peso=" + peso + ", vegano=" + vegano + ", ingredientes=" + ingredientes + "]";
	}

	@Override
	public String getTipo() {
		return "Comida vegana? "+vegano+", utilizando os ingredientes: "+ingredientes;
	}
	
	public void verificaPeso() {
		try {
			if(this.peso == null || BigDecimal.ZERO.compareTo(this.peso) == 1) {
				throw new ComidaException(this.peso);
			}
		}
		catch(ComidaException e) {
			e.printStackTrace();
		}
	}
	
	
}
