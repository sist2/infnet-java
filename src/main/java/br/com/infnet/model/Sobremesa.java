package br.com.infnet.model;

import java.math.BigDecimal;

import br.com.infnet.exceptions.ProdutoException;
import br.com.infnet.exceptions.SobremesaException;

public class Sobremesa extends Produto{
	
	private BigDecimal quantidade;
	
	private boolean doce;
	
	private String informacoes;

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public boolean isDoce() {
		return doce;
	}

	public void setDoce(boolean doce) {
		this.doce = doce;
	}

	public String getInformacoes() {
		return informacoes;
	}

	public void setInformacoes(String informacoes) {
		this.informacoes = informacoes;
	}

	@Override
	public String toString() {
		return "Sobremesa [quantidade=" + quantidade + ", doce=" + doce + ", informacoes=" + informacoes + "]";
	}

	@Override
	public String getTipo() {
		return "Sobremesa do tipo doce? " +doce+". Informações complementares: "+informacoes;
	}
	
	public void verificaQuantidade() {
		try {
			if(this.quantidade == null || BigDecimal.ZERO.compareTo(this.quantidade) == 1) {
				throw new SobremesaException(this.quantidade);
			}
		}
		catch(SobremesaException e) {
			e.printStackTrace();
		}
	}
	
	
}
