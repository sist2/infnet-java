package br.com.infnet.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.infnet.arquivo.Arquivo;

public class SobremesaTest {
	
	String nomeSobremesas = "";
	
	@Test
	public void arquivoSobremesas() throws IOException {
		
	
		Sobremesa sobremesa1 = new Sobremesa();
		Sobremesa sobremesa2 = new Sobremesa();
		Sobremesa sobremesa3 = new Sobremesa();
		
		sobremesa1.setCodigo(1);
		sobremesa1.setNome("Nome 1");
		sobremesa1.setValor(BigDecimal.TEN);
		sobremesa1.setQuantidade(BigDecimal.ONE);
		
		sobremesa2.setCodigo(1);
		sobremesa2.setNome("Nome 2");
		sobremesa2.setValor(BigDecimal.ONE);
		sobremesa2.setQuantidade(BigDecimal.ONE);
		
		sobremesa3.setCodigo(1);
		sobremesa3.setNome("Nome 3");
		sobremesa3.setValor(new BigDecimal(6));
		sobremesa3.setQuantidade(BigDecimal.TEN);
		
		List<Sobremesa> sobremesas = new ArrayList<Sobremesa>();
		
		sobremesas.add(sobremesa1);
		sobremesas.add(sobremesa2);
		sobremesas.add(sobremesa3);
		
		
		Arquivo arquivo = new Arquivo();
		arquivo.Read("src/main/resources/ArquivoSobremesas.txt");
		
		sobremesas.forEach(b-> {
			b.verificaQuantidade();
			nomeSobremesas += b.getNome() + "\n";
		});
		
		arquivo.Write("src/main/resources/ArquivoSobremesas.txt", nomeSobremesas);
	}
}
