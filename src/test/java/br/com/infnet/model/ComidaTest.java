package br.com.infnet.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.infnet.arquivo.Arquivo;

public class ComidaTest {
	
	String nomeComidas = "";
	@Test
	public void arquivoComidas() throws IOException {
		Comida comida1 = new Comida();
		Comida comida2 = new Comida();
		Comida comida3 = new Comida();
		
		comida1.setCodigo(1);
		comida1.setIngredientes("Cebola, Salsa, Carne");
		comida1.setPeso(BigDecimal.TEN);
		comida1.setNome("Nome 1");
		comida1.setValor(BigDecimal.TEN);
		
		comida2.setCodigo(1);
		comida2.setIngredientes("Cebola, Salsa, Carne");
		comida2.setNome("Nome 2");
		comida2.setPeso(BigDecimal.TEN);
		comida2.setValor(BigDecimal.ONE);
		
		comida3.setCodigo(1);
		comida3.setIngredientes("Cebola, Salsa, Carne");
		comida3.setPeso(BigDecimal.TEN);
		comida3.setNome("Nome 3");
		comida3.setValor(BigDecimal.ONE);
		
		List<Comida> comidas = new ArrayList<Comida>();
		
		comidas.add(comida1);
		comidas.add(comida2);
		comidas.add(comida3);
				
		Arquivo arquivo = new Arquivo();
		arquivo.Read("src/main/resources/ArquivoComidas.txt");
		
		comidas.forEach(c-> {
			c.verificaPeso();
			c.verificaValor();
			nomeComidas += c.getNome() + "\n";
		});
		
		arquivo.Write("src/main/resources/ArquivoBebidas.txt", nomeComidas);
	}
	
}
