package br.com.infnet.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.infnet.arquivo.Arquivo;

public class BebidaTest {
	
	String nomeBebidas = "";
	
	@Test
	public void arquivoBebidas() throws IOException {
		Bebida bebida1 = new Bebida();
		Bebida bebida2 = new Bebida();
		Bebida bebida3 = new Bebida();
		
		bebida1.setCodigo(1);
		bebida1.setGelada(true);
		bebida1.setMarca("Marca 1");
		bebida1.setNome("Nome 1");
		bebida1.setTamanho(1F);
		bebida1.setValor(BigDecimal.TEN);
		
		bebida2.setCodigo(1);
		bebida2.setGelada(true);
		bebida2.setMarca("Marca 2");
		bebida2.setNome("Nome 2");
		bebida2.setTamanho(2F);
		bebida2.setValor(BigDecimal.ONE);
		
		bebida3.setCodigo(1);
		bebida3.setGelada(true);
		bebida3.setMarca("Marca 3");
		bebida3.setNome("Nome 3");
		bebida3.setTamanho(3F);
		bebida3.setValor(new BigDecimal(6));
		
		List<Bebida> bebidas = new ArrayList<Bebida>();
		
		bebidas.add(bebida1);
		bebidas.add(bebida2);
		bebidas.add(bebida3);
		
		
		Arquivo arquivo = new Arquivo();
		arquivo.Read("src/main/resources/ArquivoBebidas.txt");
		
		bebidas.forEach(b-> {
			b.verificaMarca();
			nomeBebidas += b.getNome() + "\n";
		});
		
		arquivo.Write("src/main/resources/ArquivoBebidas.txt", nomeBebidas);
	}
}
