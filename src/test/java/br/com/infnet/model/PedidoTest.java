package br.com.infnet.model;

import java.time.LocalDateTime;

import org.junit.Test;

public class PedidoTest {
	
	@Test
	public void pedidoTeste() {
		Pedido pedido1 = new Pedido();
		Pedido pedido2 = new Pedido();
		Pedido pedido3 = new Pedido();
		Solicitante sol1 = new Solicitante("Nome1", "123.187.123-80", "eu@eu.com.br");
		Solicitante sol2 = new Solicitante("Nome2", "123.187.123-70", "eu@eu.com.br");
		Solicitante sol3 = new Solicitante("Nome3", "123.187.123-60", "eu@eu.com.br");
		
		pedido1.setDescricao("Descrição 1");
		pedido1.setWeb(false);
		pedido1.setData(LocalDateTime.of(2020, 1, 8, 0, 0));
		pedido1.setSolicitante(sol1);
		
		pedido2.setDescricao("Descrição 2");
		pedido2.setWeb(false);
		pedido2.setData(LocalDateTime.of(2020, 1, 8, 0, 0));
		pedido2.setSolicitante(sol2);
		
		pedido3.setDescricao("Descrição 3");
		pedido3.setWeb(false);
		pedido3.setData(LocalDateTime.of(2020, 1, 8, 0, 0));
		pedido3.setSolicitante(sol3);
				
		
	}
}
